from markov import get_words, insert_borders, SentenceStart, SentenceEnd, \
                   get_dict, make_sentence
import unittest


class TestGetWords(unittest.TestCase):

    def test_from_empty_text(self):
        self.assertEqual(get_words(''), [])

    def test_single_word(self):
        self.assertEqual(get_words('Fish'), ['Fish'])

    def test_multiple_words(self):
        self.assertEqual(get_words('Fish black you'), ['Fish', 'black', 'you'])

    def test_many_spaces(self):
        self.assertEqual(get_words(' Fish    black   you     '),
                         ['Fish', 'black', 'you'])


class TestInsertBorders(unittest.TestCase):

    def test_simple_sentence(self):
        self.assertEqual(insert_borders(['Fish', 'is', 'good.']),
                         [SentenceStart, 'Fish', 'is', 'good.', SentenceEnd])

    def test_two_sentences(self):
        self.assertEqual(
            insert_borders(
                ['Fish', 'is', 'good.', 'But', 'meat', 'is', 'better!']
            ),
            [SentenceStart, 'Fish', 'is', 'good.', SentenceEnd,
             SentenceStart, 'But', 'meat', 'is', 'better!', SentenceEnd]
        )

    def test_question(self):
        self.assertEqual(insert_borders(['How', 'are', 'you?']),
                         [SentenceStart, 'How', 'are', 'you?', SentenceEnd])

    def test_exclamation(self):
        self.assertEqual(insert_borders(["I am", "ok!"]),
                         [SentenceStart, "I am", "ok!", SentenceEnd])

    def test_name_in_sentence(self):
        words = ['Мой', 'товарищ', 'Петя', 'гуляет.']
        expected = [SentenceStart, 'Мой', 'товарищ', 'Петя', 'гуляет.',
                    SentenceEnd]
        self.assertEqual(insert_borders(words), expected)


class TestGetDict(unittest.TestCase):

    maxDiff = None

    def test_simple_sentence(self):
        words = [SentenceStart, 'Fish', 'is', 'good.', SentenceEnd]
        expected_size_1 = {
            (SentenceStart,): ['Fish'],
            ('Fish',): ['is'],
            ('is',): ['good.'],
            ('good.',): [SentenceEnd]
        }
        self.assertEqual(get_dict(words, 1), expected_size_1)

        expected_size_2 = {
            (SentenceStart, 'Fish'): ['is'],
            ('Fish', 'is'): ['good.'],
            ('is', 'good.'): [SentenceEnd],
        }
        self.assertEqual(get_dict(words, 2), expected_size_2)

    def test_multiple_values(self):
        words = [SentenceStart, 'One', 'fish', 'two', 'fish', 'red', 'fish',
                 'blue', 'fish.', SentenceEnd]
        expected_size_1 = {
            (SentenceStart,): ['One'],
            ('One',): ['fish'],
            ('fish',): ['two', 'red', 'blue'],
            ('fish.',): [SentenceEnd],
            ('two',): ['fish'],
            ('red',): ['fish'],
            ('blue',): ['fish.']
        }
        self.assertEqual(get_dict(words, 1), expected_size_1)

        expected_size_2 = {
            (SentenceStart, 'One'): ['fish'],
            ('One', 'fish'): ['two'],
            ('fish', 'two'): ['fish'],
            ('two', 'fish'): ['red'],
            ('fish', 'red'): ['fish'],
            ('red', 'fish'): ['blue'],
            ('fish', 'blue'): ['fish.'],
            ('blue', 'fish.'): [SentenceEnd]
        }
        self.assertEqual(get_dict(words, 2), expected_size_2)

    def test_two_sentences(self):
        words = [SentenceStart, 'How', 'are', 'you?', SentenceEnd,
                 SentenceStart, 'I', 'am', 'ok!', SentenceEnd]
        expected_size_1 = {
            (SentenceStart,): ['How', 'I'],
            ('How',): ['are'],
            ('are',): ['you?'],
            ('you?',): [SentenceEnd],
            ('I',): ['am'],
            ('am',): ['ok!'],
            ('ok!',): [SentenceEnd]
        }
        self.assertEqual(get_dict(words, 1), expected_size_1)

        expected_size_2 = {
            (SentenceStart, 'How'): ['are'],
            ('How', 'are'): ['you?'],
            ('are', 'you?'): [SentenceEnd],
            (SentenceStart, 'I'): ['am'],
            ('I', 'am'): ['ok!'],
            ('am', 'ok!'): [SentenceEnd]
        }
        self.assertEqual(get_dict(words, 2), expected_size_2)

    def two_key_specific_text(self):
        words = [SentenceStart, 'Привет,', 'как', 'дела?', SentenceEnd,
                 SentenceStart, 'Привет,', 'как', 'ты?', SentenceEnd,
                 SentenceStart, 'Привет,', 'что', 'делаешь?', SentenceEnd,
                 SentenceStart, 'У', 'вас', 'как', 'дела?', SentenceEnd]

        expected_size_2 = {
            (SentenceStart, 'Привет,'): ['как', 'как', 'что'],
            ('Привет,', 'как'): ['дела?', 'ты?'],
            ('как', 'дела?'): [SentenceEnd, SentenceEnd],
            ('как', 'ты?'): [SentenceEnd],
            ('Привет,', 'что'): ['делаешь?'],
            ('что', 'делаешь?'): [SentenceEnd],
            (SentenceStart, 'У'): ['вас'],
            ('У', 'вас'): ['как'],
            ('вас', 'как'): ['дела?']
        }
        self.assertEqual(get_dict(words, 2), expected_size_2)


class TestMakeSentence(unittest.TestCase):
    def simple_text(self):
        d = {
            (SentenceStart,): ['Раз'],
            ('два',): ['три.'],
            ('три.',): [SentenceEnd]
        }
        expected = 'Раз два три.'
        self.assertEqual(make_sentence(d), expected)


if __name__ == '__main__':
    unittest.main()
