import re
import random


def get_words(text):
    words_list = text.split()
    return [word for word in words_list]


class SentenceStart(object):
    pass


class SentenceEnd(object):
    pass


def insert_borders(words):
    new_list = [SentenceStart]
    last_word = None
    for word in words:
        if last_word == SentenceEnd and word[0].isupper():
            new_list.append(SentenceStart)

        new_list.append(word)
        last_word = word

        if re.match(r'.+[\.\!\?]', word):
            new_list.append(SentenceEnd)
            last_word = SentenceEnd
    return new_list


def get_dict(words, key_size):
    words_dict = {}
    for i in range(0, len(words) - key_size):
        tup = words[i:i+key_size+1]
        key = tuple(tup[:-1])
        value = tup[-1]
        if SentenceEnd in key:
            continue
        if key in words_dict:
            words_dict[key].append(value)
        else:
            words_dict[key] = [value]
    return words_dict


def prepare_text(text, key_size=1):
    words_list = get_words(text)
    words_list_with_borders = insert_borders(words_list)
    return get_dict(words_list_with_borders, key_size)


def get_next_word(words_dict, prev_key):
    if prev_key not in words_dict:
        return SentenceEnd
    available_words = words_dict[prev_key]
    return random.choice(available_words)


def make_sentence(words_dict):
    words_dict_starts = [key for key in words_dict.keys()
                         if key[0] == SentenceStart]
    first_key = random.choice(words_dict_starts)
    sentence = list(first_key[1:])
    key = first_key
    key_size = len(key)
    while SentenceEnd not in key:
        sentence.append(get_next_word(words_dict, key))
        key = tuple(sentence[-key_size:])
    return ' '.join(sentence[:-1])
