from markov import prepare_text, make_sentence


def main():
    with open('text.txt', 'r') as file:
        text = file.read()
    words_dict = prepare_text(text, 2)
    s = make_sentence(words_dict)
    print(s)


if __name__ == '__main__':
    main()
