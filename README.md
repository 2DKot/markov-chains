
```
Texts generator, based on Markov chains.
```

This project was made for educational purposes.
It isn't optimized and may be slow on big amount of text.

## Run:
``` bash
  python3 main.py
```

You can change base text in ```text.txt``` file.

## Tests:
``` bash
  python3 tests.py
```
